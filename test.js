/*eslint-env node, mocha */

let chai = require('chai');
let should = chai.should();

let { isBetterThan, hand, HANDS, VALUES } = require('./hand.js');



describe('hand parser should identify a', () => {
	it('Royal Flush', () => {
		let h = hand(['10D', 'JD', 'QD', 'KD', 'AD']);
		h.type.should.equal(HANDS.STRAIGHT_FLUSH);
		h.value.should.equal(VALUES.ACE);
		should.equal(h.highest, null);
	});

	it('Straight Flush', () => {
		let h = hand(['9D', '10D', 'JD', 'QD', 'KD']);
		h.type.should.equal(HANDS.STRAIGHT_FLUSH);
		should.equal(h.highest, null);
	});

	it('Four of a Kind', () => {
		let h = hand(['9D', '9H', '9C', '9S', '8D']);
		h.type.should.equal(HANDS.FOUR_OF_A_KIND);
		should.equal(h.highest, null);
	});

	it('Full House', () => {
		let h = hand(['9D', '9H', '9C', '8D', '8H']);
		h.type.should.equal(HANDS.FULL_HOUSE);
		// in full house, the value of the three of a kind counts
		h.value.should.equal(VALUES.NINE);
		should.equal(h.highest, null);
	});

	it('Flush', () => {
		let h = hand(['9D', '8D', '2D', '3D', 'AD']);
		h.type.should.equal(HANDS.FLUSH);
		h.value.should.equal(VALUES.ACE);
		should.equal(h.highest, null);
	});

	it('Straight', () => {
		let h = hand(['2D', '3S', '4D', '5D', '6D']);
		h.type.should.equal(HANDS.STRAIGHT);
		h.value.should.equal(VALUES.SIX);
		should.equal(h.highest, null);
	});

	it('Straight with a low ace', () => {
		let h = hand(['2D', '3S', '4D', '5D', 'AD']);
		h.type.should.equal(HANDS.STRAIGHT);
		h.value.should.equal(VALUES.FIVE);
		should.equal(h.highest, null);
	});

	it('Three of a Kind', () => {
		let h = hand(['2D', '2H', '2S', '3C', '4D']);
		h.type.should.equal(HANDS.THREE_OF_A_KIND);
		h.value.should.equal(VALUES.TWO);
		should.equal(h.highest, null);
	});

	it('Two Pairs', () => {
		let h = hand(['2D', '2H', '3C', '3D', '5D']);
		h.type.should.equal(HANDS.TWO_PAIRS);
		h.value.should.equal(VALUES.THREE);
		h.highest.should.equal(VALUES.FIVE);
	});

	it('One Pair', () => {
		let h = hand(['2D', '2H', '3C', '6D', '9D']);
		h.type.should.equal(HANDS.ONE_PAIR);
		h.value.should.equal(VALUES.TWO);
		h.highest.should.equal(VALUES.NINE);
	});

	it('High Card', () => {
		let h = hand(['2D', '3H', '5C', '8H', '9S']);
		should.equal(h.type, null);
		should.equal(h.value, null);
		h.highest.should.equal(VALUES.NINE);
	});
});

describe('isBetterThan()', () => {
	it('should choose Royal Flush over Flush', () => {
		isBetterThan( ['10D', 'JD', 'QD', 'KD', 'AD']
		            , ['9D', '8D', '2D', '3D', 'AD']
	                ).should.equal(true);
	});

	it('should choose Three of a Kind over Two Pairs', () => {
		isBetterThan( ['2D', '2H', '2S', '3C', '4D']
		            , ['2D', '2H', '3C', '3D', '5D']
	                ).should.equal(true);
	});

	it('should not choose if they are the same', () => {
		should.equal( isBetterThan( ['2D', '2H', '2S', '3C', '4D']
		                          , ['2D', '2H', '2S', '3C', '4D']
	                              )
		            , null);
	});
});
