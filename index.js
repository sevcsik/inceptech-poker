#!/bin/env node

let { isBetterThan, hand } = require('./hand.js');
let readline = require('readline');

let rl = readline.createInterface({ input: process.stdin
                                  , output: process.stdout
                                  , terminal: false
                                  });

let count = null;
rl.on('line', (line) => {
	if (count === null) {
		count = +line;
	} else {
		count--;
		let cards = line.split(' ');

		console.log(cards);

		if (isBetterThan(cards.slice(0, 4), cards.slice(5, 9))) {
			console.log('Player 2');
		} else {
			console.log('Player 1');
		}

		if (count === 0) {
			rl.close();
		}
	}
});


