const HANDS = { ONE_PAIR: 1
              , TWO_PAIRS: 2
              , THREE_OF_A_KIND: 3
              , STRAIGHT: 4
              , FLUSH: 5
              , FULL_HOUSE: 6
              , FOUR_OF_A_KIND: 7
              , STRAIGHT_FLUSH: 8
              };

const VALUES = { ONE: 1
               , TWO: 2
               , THREE: 3
               , FOUR: 4
               , FIVE: 5
               , SIX: 6
               , SEVEN: 7
               , EIGHT: 8
               , NINE: 9
               , TEN: 10
               , JACK: 11
               , QUEEN: 12
               , KING: 13
               , ACE: 14
               };

let toValue = (card, aceIsOne) => {
	let v = card.length === 2 ? card[0] : "10";

	if(isNaN(+v)) {
		switch (v) {
			case "J": return VALUES.JACK;
			case "Q": return VALUES.QUEEN;
			case "K": return VALUES.KING;
			case "A": return aceIsOne ? VALUES.ONE : VALUES.ACE;
			default: throw new Error('invalid value: ' + v);
		}
	} else {
		return +v;
	}
};

let toColour = (card) => {
	return card[card.length - 1];
};

let getStraight = (cards) => {
	let variations = [false, true].map((aceIsOne) => {
		let cards_ = cards.slice(0, 5).sort((a, b) => toValue(a, aceIsOne) - toValue(b, aceIsOne));

		let i;
		for (i = 0; i < cards_.length; i++) {
			if (i < 1) continue;
			if ( toValue(cards_[i], aceIsOne)
			   - toValue(cards_[i - 1], aceIsOne)
			   !== 1
			   ) break;
		}

		return i === cards_.length ? toValue(cards_[cards_.length - 1], aceIsOne) : null;
	});
	return Math.max(...variations);
};

let isFlush = (cards) => {
	let colour;
	for (let card of cards) {
		if (!colour) colour = toColour(card);
		else if (colour !== toColour(card)) return false;
	}
	return true;
};

let getNOfAKinds = (cards) => {
	let ret = { '2': [], 3: [], 4: [] };

	for (let kind in VALUES) {
		let result = cards.filter((e) => toValue(e) === VALUES[kind]);
		if (result.length >= 2) {
			ret[result.length].push(VALUES[kind]);
		}
	}

	return ret;
};

let filterOutValues = (values, cards) => {
	cards = cards.map((e) => toValue(e));

	for (let value of values) {
		cards = cards.filter((e) => e !== value);
	}

	return cards;
};

let hand = (cards) => {
	// order ascending by value
	let value = getStraight(cards);
	if (value) {
		return { type: isFlush(cards) ? HANDS.STRAIGHT_FLUSH : HANDS.STRAIGHT
		       , value
		       , highest: null
		};
	}

	let nOfAKinds = getNOfAKinds(cards);
	if (nOfAKinds['4'].length) {
		return { type: HANDS.FOUR_OF_A_KIND
		       , value: nOfAKinds['4'][0]
	           , highest: null // it's not possible to have two four of a kinds with the same value
		       };
	}

	if (nOfAKinds['3'].length) {
		if (nOfAKinds['2'].length) {
			return { type: HANDS.FULL_HOUSE
			       , value: nOfAKinds['3'][0]
			       , highest: null
		           };
		} else {
			return { type: HANDS.THREE_OF_A_KIND
			       , value: nOfAKinds['3'][0]
                   , highest: null
			       };
		}
	}

	if (nOfAKinds['2'].length === 2) {
		return { type: HANDS.TWO_PAIRS
		       , value: Math.max(nOfAKinds['2'][0], nOfAKinds['2'][1])
			   , highest: filterOutValues(nOfAKinds['2'], cards)[0]
		       };
	}

	let cards_ = cards.slice(0, 5).sort((a, b) => toValue(a) - toValue(b));

	if (nOfAKinds['2'].length === 1) {
		return { type: HANDS.ONE_PAIR
		       , value: nOfAKinds['2'][0]
	           , highest: Math.max(...filterOutValues(nOfAKinds['2'], cards_))
               };
    }

	if (isFlush(cards)) {
		return { type: HANDS.FLUSH
		       , value: Math.max(...cards.map((e) => toValue(e)))
	           , highest: null
               };
    }

    return { type: null
	       , value: null
	       , highest: Math.max(...cards.map((e) => toValue(e)))
	       };
};

let isBetterThan = (hand1, hand2) => {
	let h1 = hand(hand1);
	let h2 = hand(hand2);

	return h1.type !== h2.type
		   ? h1.type > h2.type
		   : h1.value !== h2.value
		     ? h1.value > h2.value
		     : h1.highest !== h2.highest
		       ? h1.highest > h2.highest
		       : null;
};

module.exports = { isBetterThan
                 , hand
                 , HANDS
                 , VALUES
                 };
